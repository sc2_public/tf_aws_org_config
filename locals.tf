locals {
  common_prefixes = data.aws_s3_bucket_objects.config.common_prefixes
}

locals {
  config_accounts = [
    for p in local.common_prefixes: trimprefix(trimsuffix(p,"/"),"AWSLogs/")
  ]
  all_accounts = [
    for a in data.aws_organizations_organization.main.accounts: a.id
  ]
}

locals {
  no_config_accounts = setsubtract(
    toset(local.all_accounts),
    toset(local.config_accounts))
}
