provider "aws" {
  alias = "audit"
}

#----- ORG CONFIG RULES ----

resource "aws_config_organization_managed_rule" "ebs_encryption" {
  name              = "ebs_encryption"
  description       = "Checks whether EBS volumes that are in an attached state are encrypted"
  rule_identifier   = "ENCRYPTED_VOLUMES"
  excluded_accounts = local.no_config_accounts
}

resource "aws_config_organization_managed_rule" "s3_encryption" {
  name              = "s3_encryption"
  description       = "Checks whether the S3 bucket policy denies put-object requests that are not encrypted using AES-256 or AWS KMS"
  rule_identifier   = "S3_BUCKET_SERVER_SIDE_ENCRYPTION_ENABLED"
  excluded_accounts = local.no_config_accounts
}

resource "aws_config_organization_managed_rule" "s3_no_public_write" {
  name              = "s3_no_public_write"
  description       = "Checks whether the S3 bucket Block Public Access settings deny public writes"
  rule_identifier   = "S3_BUCKET_PUBLIC_WRITE_PROHIBITED"
  excluded_accounts = local.no_config_accounts
}

resource "aws_config_organization_managed_rule" "rds_encryption" {
  name              = "rds_encryption"
  description       = "Checks whether RDS instances are encrypted"
  rule_identifier   = "RDS_STORAGE_ENCRYPTED"
  excluded_accounts = local.no_config_accounts
}

resource "aws_config_organization_managed_rule" "efs_encryption" {
  name              = "efs_encryption"
  description       = "Checks whether EFS filesystems are encrypted"
  rule_identifier   = "EFS_ENCRYPTED_CHECK"
  excluded_accounts = local.no_config_accounts
}

resource "aws_config_organization_managed_rule" "root_no_access_keys" {
  name              = "root_no_access_keys"
  description       = "Checks whether root user has access keys"
  rule_identifier   = "IAM_ROOT_ACCESS_KEY_CHECK"
  excluded_accounts = local.no_config_accounts
}

#
# excluded 2020-09-25 - automation tools usually don't use MFA
#
#resource "aws_config_organization_managed_rule" "iam_users_mfa_required" {
#  name              = "iam_users_mfa_required"
#  description       = "Checks whether IAM users have MFA enabled"
#  rule_identifier   = "IAM_USER_MFA_ENABLED"
#  excluded_accounts = local.no_config_accounts
#}

resource "aws_config_organization_managed_rule" "iam_users_console_mfa_required" {
  name              = "iam_users_console_mfa_required"
  description       = "Checks whether IAM users have MFA enabled for console access"
  rule_identifier   = "MFA_ENABLED_FOR_IAM_CONSOLE_ACCESS"
  excluded_accounts = local.no_config_accounts
}

resource "aws_config_organization_managed_rule" "restricted_access" {
  name              = "restricted_access"
  description       = "Checks whether any security groups with inbound 0.0.0.0/0 have TCP or UDP ports accessible. The rule is NON_COMPLIANT when a security group with inbound 0.0.0.0/0 has a port accessible which is not specified in the rule parameters."
  rule_identifier   = "VPC_SG_OPEN_ONLY_TO_AUTHORIZED_PORTS"
  input_parameters  = "{\"authorizedTcpPorts\": \"80,443\"}"
  excluded_accounts = local.no_config_accounts
}

#resource "aws_config_organization_managed_rule" "restricted_ssh" {
#  name              = "restricted_ssh"
#  description       = "Checks whether the incoming SSH traffic for the security groups is accessible. The rule is COMPLIANT when the IP addresses of the incoming SSH traffic in the security groups are restricted. This rule applies only to IPv4."
#  rule_identifier   = "INCOMING_SSH_DISABLED"
#  excluded_accounts = local.no_config_accounts
#}

resource "aws_config_organization_managed_rule" "vpc_flowlogs_enabled" {
  name              = "vpc_flowlogs_enabled"
  description       = "Checks whether VPC Flow Logs are enabled"
  rule_identifier   = "VPC_FLOW_LOGS_ENABLED"
  input_parameters  = "{\"trafficType\": \"ALL\"}"
  excluded_accounts = local.no_config_accounts
}

