data "aws_organizations_organization" "main" {}

data "aws_s3_bucket" "config" {
  bucket   = "${data.aws_organizations_organization.main.id}-config"
  provider = aws.audit
}

data "aws_s3_bucket_objects" "config" {
  bucket    = data.aws_s3_bucket.config.id
  delimiter = "/"
  prefix    = "AWSLogs/"
  provider  = aws.audit
}

